# -*- coding: utf-8 -*-
import numpy as np
import cv2
import gopigo as go
import time
import math
import RPi.GPIO as GPIO
from pump_out import pump_out_of
from pump_in import pump_into
from pump_prepare import pump_prepare_of
from servo_down import servo_downside
from servo_up import servo_upside
# global variable for determining gopigo speed
gospeed = 220
water_tank = 0
go.enable_servo()

GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
 
RELAIS_1_GPIO = 20
RELAIS_2_GPIO = 21

GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_2_GPIO, GPIO.OUT) # GPIO Assign mode
# global variable for video feed
cap = None


def init():
    global cap, gospeed, RELAIS_1_GPIO, RELAIS_2_GPIO
    # This function should do everything required to initialize the robot.
    # Among other things it should open the camera and set gopigo speed.
    # Some of this has already been filled in.
    # You are welcome to add your own code, if needed.
    
    cap = cv2.VideoCapture(0)
    go.set_speed(gospeed)
    return
# TASK 1
def get_line_location(frame):
    #original colour image to hsv
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #cutting the frame
    r = [len(frame)-220, len(frame)-200, 0, len(frame[0])]
    frame = frame[r[0]:r[1], r[2]:r[3]]
    
#    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    trackbar_value_lb = 41
    trackbar_value_lg = 71
    trackbar_value_lr = 5
    trackbar_value_hb = 151
    trackbar_value_hg = 255
    trackbar_value_hr = 249
    
    lowerLimits = np.array([trackbar_value_lb, trackbar_value_lg, trackbar_value_lr])
    upperLimits = np.array([trackbar_value_hb, trackbar_value_hg, trackbar_value_hr])
    threshold = cv2.inRange(frame, lowerLimits, upperLimits)
    #nonzero takes x,y coordinate from threshold
    #np.mean calculate avg value from nonzero x coordinates
    #round - makes values to int
    coordinat = np.nonzero(threshold)
    coordinat_x = np.mean(coordinat[1])
    coordinat_round = round(coordinat_x, 0)
    
    cv2.imshow('Origin', threshold)
    
            
    return coordinat_round

def bang_bang_with_hysteresis(linelocation):
    if not math.isnan(linelocation):
        if linelocation:
            #middle in range (240, 400)
            #global speed 180
            if linelocation < 280:
                go.set_right_speed(200)
                go.set_left_speed(230)
                go.forward()
            elif linelocation > 360:
                go.set_right_speed(230)
                go.set_left_speed(200)
                go.forward()
            elif linelocation in range(280, 360):
                go.set_speed(gospeed)
                go.forward()
    return
def water_pump():
    print('here is code for pump')
    go.stop()
    time.sleep(2)
    go.set_speed(220)
    go.right_rot()
    time.sleep(1)
    go.stop()
    
    #servo_downside()
    
    pump_prepare_of()
    pump_into()
    pump_prepare_of()
    
    #servo_upside()
    
    go.set_speed(220)
    go.right_rot()
    time.sleep(1)
    go.stop()

    return


# Initialization
init()

while True:
    # We read information from camera.
    ret, frame = cap.read()
    #cv2.imshow('Original', frame)
    
    # Task 1: uncomment the following line and implement get_line_location function.
    linelocation = get_line_location(frame)
    print(linelocation)
    
    # Task 3: uncomment the following line and implement bang_bang_with_hysteresis function.
    if not math.isnan(linelocation):
        bang_bang_with_hysteresis(linelocation)
    elif math.isnan(linelocation):
        water_pump()
        break
        
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
go.stop()
