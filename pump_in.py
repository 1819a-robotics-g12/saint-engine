import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
 
RELAIS_1_GPIO = 20 #out
RELAIS_2_GPIO = 21 #in

GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_2_GPIO, GPIO.OUT) # GPIO Assign mode

def pump_into():
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # on out
    time.sleep(2)
    GPIO.output(RELAIS_2_GPIO, GPIO.LOW)  # on in
    GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # off out 
    time.sleep(14)# on
    GPIO.output(RELAIS_2_GPIO, GPIO.HIGH) # off in
    return
pump_into()
