import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(33,GPIO.OUT)
GPIO.setup(35,GPIO.OUT)

def pump_in():
    print("pump1 is on")
    GPIO.output(33,GPIO.HIGH)
    time.sleep(4)
    print("pump1 is off")
    GPIO.output(33,GPIO.LOW)
    return
    
def pump_out():
    print("pump2 is on")
    GPIO.output(35,GPIO.HIGH)
    time.sleep(4)
    print("pump2 is off")
    GPIO.output(35,GPIO.LOW)
    return
    
#pump_in()
#pump_out()
