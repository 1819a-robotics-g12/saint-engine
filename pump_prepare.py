import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
 
RELAIS_1_GPIO = 20
RELAIS_2_GPIO = 21

GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_2_GPIO, GPIO.OUT) # GPIO Assign mode

def pump_prepare_of():
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # on
    GPIO.output(RELAIS_2_GPIO, GPIO.LOW)
    time.sleep(10)# on
    GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # ooff
    GPIO.output(RELAIS_2_GPIO, GPIO.HIGH)
    return
pump_prepare_of()

