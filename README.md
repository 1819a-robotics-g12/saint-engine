<<<<<<< HEAD
# Fire Engine
## Martin Rahuoja
## Timofei Ganjusev
## Kristo Vanderflit

## Overview
### Project Fire Engine
Project Fire Engine focuses on an everyday problem that many of our proud men and women earn their living solving - firefighting. The aim of our project is to make a miniature example of what could 
possibly (although not likely) be used to automate the process of pumping water from a reservoir and bringing it to the place of emergency. We plan to construct a robot with a watertank on top which
navigates a blue line between two small reservoirs, using a webcam, one full of water and the other empty. The robot approaches one of the reservoirs, turns towards it, checks whether it has water or not 
and decides, how to act. If the reservoir is empty - the ultrasonic sensor hovering above the container doesn't detect anything in it, it goes to the other reservoir, if it's not, it lowers a hose into 
the reservoir using a servo motor, pumps some water into its tank, turns around and goes to the other end of the line where another pump unleashes the water out into the secondary container.

## Schedule for Pair A
Because of the departure of our comrade, Kaspar Treimuth, we no longer have pair A and pair B, we work as a unit.

## Schedule
### Before 27.11.18
* Get the robot to follow the line and navigate the two containers
* Get the robot driving
* Figure out how to get data from the ultrasonic sensor
* Figure how to control nopwm servo motor
* Actually build the robot
* FIgure out how to connect and control water pumps to Raspberry Pi

### Before 12.12.18
* Make the robot look presentable
* Market the idea
* Photoshoot with robot
* Put the actual poster together
* Convert the poster into pdf
* Spam posters all over campus walls

### Before 17.12.18
* Put all the bits of code and wiring and elements and stuff together
* Make sure the robot actually works and does what it's supposed to
* Debug if necessary (probably is)
* Waterproofing
* Brew lots of coffee for the allnighter

## Component list
|Item       |Amount|
|-----------|------|
|Servo      |3     |
|Ultrasonic HC-SR04 |1|
|Breadboard |1|
|Webcam |1|
|Battery |1|
|Pump |2|
|Cables |40(1 package)|
|L293D Controller|1|
|GoPiGo platform (without GoPiGo board) |1|
|clotheshanger|1|
|pen|1|
>>>>>>> 0d7d3bdfc3258015947d9ef5b7a505f54eb4140
