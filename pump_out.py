import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
RELAIS_1_GPIO = 20 #out
GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode

def pump_out_of():
    GPIO.output(RELAIS_1_GPIO, GPIO.LOW) # on out
    time.sleep(1)
    GPIO.output(RELAIS_1_GPIO, GPIO.HIGH) # off out
    return
pump_out_of()
